<html>
<head>
<style>

header { 
	height: 90px;
	width: 100%;
	padding: 5px;
	color: #ffffff;
	background-color: #ff5c33;
	margin: 46px 0px 0px 0px;
}

h1 {
	font-family: Comic Sans MS;
	letter-spacing: 6px;
	font-size: 80px;
	text-align: left;
		
}

nav {
	float: left;
	width: 100%;	
}
nav ul {
	list-style-type: none;	
	overflow: hidden;
	padding: 0px;
	margin: 0px;
	background-color: #333;
	position: fixed;
   	top: 0;
	width: 1366px;
	margin-left:0px;
}

nav li {
    	float: left;
}

nav li a {
    	display: block;
    	color: white;
    	text-align: center;
    	padding: 14px 16px;
	text-decoration: none;
}

nav li a:hover:not(.active) {
	background-color: #a6a6a6;
}

nav li a:hover:active {
	background-color: #ff5c33;
}
label {
	display:inline-block;
	width:120px;
	margin-bottom:40px;
	color: white;
	font-size: 17px;
}

div {
	width: 500px;
	height: 500px;
	margin-top: 100px;
	margin-bottom: 100px;
	background-color: #333;
}
	
</style>
</head>

<body style="background-color: #8c8c8c; margin: 0px;">

<header>
	<h1 style="display: inline;"><img src="book.png"><b>BIBLIOTHECA</b>
	<span style="float:right; font-size:20px;letter-spacing:0px; padding: 25px; font-family: Courier New;">
		Your link to the past & gateway to the future</span></h1>

</header>


<!--Navigation Bar-->

<nav>
<ul>
  	<li><a href="#home">Home</a></li>
  	<li><a href="#news">About Us</a></li>
  	<li><a href="#help">Help</a></li>
  	<li><a href="#contact">Contact</a></li>
  	<li><a href="#contact">Advanced Search</a></li>
  
</ul>
</nav>
<center>
<div>
<center>
<form method="post" action="process.php" style="padding: 50px;">
<label>Username</label>
<input type="text" name="username" style="padding: 5px;"/>
<br />
<label>Payment Method</label>
  <input list="pay-method" name="payment_method" style="padding: 7px; width: 180px;">
	<datalist id="pay-method">
    <option value="Visa Card">
    <option value="Master Card">
    <option value="Other">
  </datalist>
<br />
<label>Card Number</label>
<input type="text" name="card_num" style="padding: 5px;"/>
<br />
<label>Security Code</label>
<input type="password" name="security_code" style="padding: 5px;" />
<br />
<label>Expiry Date</label>
  <input type="date" name="exp" style="padding: 7px; width: 180px;">
<br />
<label>Email</label>
<input type="text" name="email" style="padding: 5px;"/>
<center><input type="submit" value="Pay Now" style="padding: 10px; background-color: #ff5c33; outline:none; border: none">
</center>
</form>
 </center>
</div> 
 </center>
</body>
</html>